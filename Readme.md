# Switch Tester!

## Usage:

1. Plug the device in
1. Place the sensor cap face up
1. Place the keycap and calibration weight on the exposed sensor
1. Press the calibrate button (pin 2), it should print out the fsr reading over the keyboard
1. Remove the weight
1. Repeat last three steps 4 times
1. Press the zero button (pin 3), it should print out the zeroed value.
1. Put the keyswitch with the keycap on in the holder
1. Rubber band the sensor cap in place on top
1. Open a text document on the computer the device is plugged into
1. Press the test button (pin 4), it should print the results into your text document as a CSV
1. Profit????

## Assembly:

### Required Hardware:

- Arduino pro micro
- Laser cut case (see svg)
- [Stepper Motor and Controller](https://smile.amazon.com/gp/product/B019TOJRC4/)
- [Force Pressure Sensor](https://www.sparkfun.com/products/8713)
- 4 10k Resistors
- 3 Momentary Buttons
- Misc wire I guess?
- 10-32 hex cap Screw and 10-32 Blind Nut Inserts
- Rubber band
- 16x 4mm ball bearings
- PFTE Lubricant (like super lube)
- 100g Calibration Weight
- Spare 1u dsa keycap

### Steps:

#### 1. Program the Arduino

1. Install the Arduino IDE
2. Open `button_pusher.ino` sketch in the IDE
3. Add the SparkFun boards under preferences -> Board manager Urls - https://raw.githubusercontent.com/sparkfun/Arduino_Boards/master/IDE_Board_Manager/package_sparkfun_index.json
4. In the Board Manager, install the manager for the Pro Micro
5. After plugging in the Pro Micro, use the IDE to flash the board

#### 2. Assemble the Device

1. Using the provided svg, use a laser to cut the case parts
1. Assemble the sensor cap
1. Assemble the stepper and screw and stuff
1. Write these steps I guess...
1. Profit???
