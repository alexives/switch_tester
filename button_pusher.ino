#include <Keyboard.h>
#include <Stepper.h>

#define STEPS 64
#define FSR_PIN 0 // the FSR and 10K pulldown are connected to a0
#define CALIBRATE_BUTTON_PIN 2
#define ZERO_BUTTON_PIN 3
#define BEGIN_BUTTON_PIN 4

Stepper stepper(STEPS, 8, 9, 10, 11);

int fsrReading;
int stepsCount = 0;
bool reverse = false;
int calibrateReading = 0;
int zeroReading = 0;

void setup(void) {
  Keyboard.print("Raw Steps,Distance MM,Raw FSR Reading,Grams");
}

void executeTest(void) {
  bool buttonState = digitalRead(BEGIN_BUTTON_PIN);

  if (buttonState == HIGH) {
    fsrReading = analogRead(FSR_PIN);
    if(fsrReading > 350) {
      reverse = true;
    }
  
    while(!reverse || fsrReading > (10 + zeroReading)) {
      // Always calibrate with 100g
      double forceGrams = 100 / calibrateReading * (fsrReading - zeroReading);
      double distanceMM = 0.79375 * 0.015625 * stepsCount;
    
      Keyboard.println(sprintf("%d,%f,%d,%f",stepsCount,distanceMM,fsrReading,forceGrams));
    
      if(reverse) {
        stepper.step(-5);
        stepsCount -= 5;
      } else {
        stepper.step(5);
        stepsCount += 5;
      }
    
      delay(200);
    }
  }
}

void calibrate(void) {
  bool buttonState = digitalRead(CALIBRATE_BUTTON_PIN);
  if (buttonState == HIGH) {
    calibrateReading = analogRead(FSR_PIN);
    Keyboard.println(calibrateReading);
    delay(500);
  }
}

void zero(void) {
  bool buttonState = digitalRead(ZERO_BUTTON_PIN);
  if (buttonState == HIGH) {
    zeroReading = analogRead(FSR_PIN);
    Keyboard.println(zeroReading);
    delay(500);
  }
}

void loop(void) {
  calibrate();
  zero();
  executeTest();
}
